package com.compliance.portlet;

import com.compliance.constants.CompliancePortletKeys;
import com.compliance.holders.AKIReasons;
import com.compliance.holders.Approvals;
import com.compliance.holders.AssignmentStatus;
import com.compliance.holders.Intermediaries;
import com.compliance.holders.Organizations;
import com.compliance.holders.Positions;
import com.compliance.holders.ProcessFlow;
import com.compliance.service.model.Compliance;
import com.compliance.service.model.UserComment;
import com.compliance.service.service.ComplianceLocalService;
import com.compliance.service.service.ComplianceLocalServiceUtil;
import com.compliance.service.service.UserCommentLocalService;
import com.compliance.service.service.UserCommentLocalServiceUtil;
import com.compliance.utils.ComplianceUtils;
import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author xmutant
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=Compliance",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=Compliance", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.name=" + CompliancePortletKeys.COMPLIANCE,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class CompliancePortlet extends MVCPortlet {
	@Reference
	ComplianceLocalService complianceLocalService;
	@Reference
	CounterLocalService counterLocalService;

	@Reference
	UserCommentLocalService userCommentLocalService;

	List<Organizations> childOrganizationsList = new ArrayList<>();

	// signed in user profile details
	long userPositionID;
	long userOrgID;
	String userPositionName;
	Map<Object, Object> map = new HashMap<Object, Object>();
	String selectedMenu;
	private Log log = LogFactoryUtil.getLog(this.getClass().getName());

	/* invoked every time page refreshes/submits */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		long selectedIntermediaryPositionId = 0;

		// lists to hold data from web services
		List<ProcessFlow> process = new ArrayList<>();
		List<Intermediaries> intermediariesList = new ArrayList<>();
		List<Approvals> approvalsList = new ArrayList<>();
		List<Organizations> organizationsList = new ArrayList<>();
		List<Positions> positionsList = new ArrayList<>();
		List<AssignmentStatus> assignmentStatusList = new ArrayList<>();
		List<AKIReasons> akiReasonsList = new ArrayList<>();
		List<Organizations> regionOrganizationsList = new ArrayList<>();

		System.out.println("________________render method called_");

		// default value is given as "" i.e empty
		selectedMenu = ParamUtil.getString(renderRequest, "selectedMenu", "");

		PortletSession session = renderRequest.getPortletSession();

		// init session with selectedMenu when selectedMenu is given
		if (!selectedMenu.equals("")) {
			session.setAttribute("selectedMenu", selectedMenu, PortletSession.PORTLET_SCOPE);
			System.out.println("________________selectMenu is NOT empty, set in session");
		}

		// when menu is not provided, use value in session if present
		if ((selectedMenu.equals(""))
				&& ((String) session.getAttribute("selectedMenu", PortletSession.PORTLET_SCOPE) != null)) {
			System.out.println("________________selectMenu is empty, set from session");
			selectedMenu = (String) session.getAttribute("selectedMenu", PortletSession.PORTLET_SCOPE);
		}

		System.out.println("________________selectedMenu_:" + selectedMenu);

		if ((User) renderRequest.getAttribute(WebKeys.USER) != null) {

			User user = (User) renderRequest.getAttribute(WebKeys.USER);
			String cNumber = (String) user.getExpandoBridge().getAttribute("fa-employee-number");

			System.out.println("________________render_profile_contract_number_is : " + cNumber);

			/* retrieves current logged in user details */
			Map<String, Object> map = ComplianceUtils.createCurrentUserProfile(cNumber);
			userPositionID = (long) map.get("user_position_id");
			userOrgID = (long) map.get("user_organization_id");
			userPositionName = (String) map.get("user_position_name");

			try {

				// set session attr only when they are null to user profile values to begin
				// with, otherwise we use selected values from drill down action

				if ((session.getAttribute("selectedOrgId", PortletSession.PORTLET_SCOPE) == null)) {
					session.setAttribute("selectedOrgId", userOrgID, PortletSession.PORTLET_SCOPE);
					session.setAttribute("selectedPositionName", userPositionName, PortletSession.PORTLET_SCOPE);
				}

				// retrieve current attributes for use in getting new lists
				long orgId = (Long) session.getAttribute("selectedOrgId", PortletSession.PORTLET_SCOPE);
				String positionName = (String) session.getAttribute("selectedPositionName",
						PortletSession.PORTLET_SCOPE);

				// retrieve selectedIntermediaryPositionId
				if ((session.getAttribute("selectedIntermediaryPositionId", PortletSession.PORTLET_SCOPE) != null)) {
					selectedIntermediaryPositionId = (long) session.getAttribute("selectedIntermediaryPositionId",
							PortletSession.PORTLET_SCOPE);
				}

				/* determine which lists to load */
				switch (selectedMenu) {
				case "postRequests": {

					process = ComplianceUtils.createProcessFlow(userPositionID, selectedIntermediaryPositionId);
					intermediariesList = ComplianceUtils.createIntermediaries(orgId, positionName);
					positionsList = ComplianceUtils.createPositions();
					assignmentStatusList = ComplianceUtils.createAssignmentStatus();
					akiReasonsList = ComplianceUtils.createAKIReasons();
					regionOrganizationsList = ComplianceUtils.createOrganizations(
							"http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getparentorg/", 3);
					organizationsList = ComplianceUtils
							.createOrganizations("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/orgs/", 0);

					renderRequest.setAttribute("process", process);
					renderRequest.setAttribute("intermediariesList", intermediariesList);
					renderRequest.setAttribute("positionsList", positionsList);
					renderRequest.setAttribute("assignmentStatusList", assignmentStatusList);
					renderRequest.setAttribute("akiReasonsList", akiReasonsList);
					renderRequest.setAttribute("regionOrganizationsList", regionOrganizationsList);
					renderRequest.setAttribute("organizationsList", organizationsList);

					break;
				}
				case "requestsApproval": {

					List<Compliance> complianceList = new ArrayList<>();
					complianceList = ComplianceUtils
							.filterProcesList(ComplianceLocalServiceUtil.findByApprovalLevel("Level1"), userPositionID);

					renderRequest.setAttribute("complianceList", complianceList);

					System.out.println(
							"________________gets_to_requestApprovals list_size_is  : " + complianceList.size());

					break;

				}
				case "requestsOnHold": {
					PortletSession ses = renderRequest.getPortletSession();
					System.out.println(
							"__________________________gets to getComplianceID from ses from render_______________________________________________ : "
									+ ses.getAttribute("newSelectedComplianceId", PortletSession.PORTLET_SCOPE));

					long complianceVal = 0;
					if ((ses.getAttribute("newSelectedComplianceId", PortletSession.PORTLET_SCOPE) != null)) {
						complianceVal = (long) ses.getAttribute("newSelectedComplianceId",
								PortletSession.PORTLET_SCOPE);
					}

					int newComplianceId = (int) complianceVal;

					List<Compliance> complianceList = ComplianceUtils.filterProcesList(
							ComplianceLocalServiceUtil.findByApprovalLevel("MoreInfo"), userPositionID);
					List<Compliance> completedList = ComplianceUtils.filterProcesList(
							ComplianceLocalServiceUtil.findByApprovalLevel("Completed"), userPositionID);
					List<UserComment> userCommentList = new ArrayList<UserComment>();
					if (completedList != null && completedList.size() > 1) {
						userCommentList = UserCommentLocalServiceUtil.findByComplianceId(newComplianceId);
					}
					renderRequest.setAttribute("complianceList", complianceList);
					renderRequest.setAttribute("completedList", completedList);
					renderRequest.setAttribute("userCommentList", userCommentList);

					break;

				}
				case "approvedRequests": {
					List<Compliance> requestApprovedList = ComplianceLocalServiceUtil.findByRequestApprovals("Accepted",
							"P");
					renderRequest.setAttribute("requestApprovedList", requestApprovedList);

					break;

				}
				case "rejectedRequests": {
					List<Compliance> requestRejectedList = ComplianceLocalServiceUtil.findByRequestApprovals("Rejected",
							"P");
					renderRequest.setAttribute("requestRejectedList", requestRejectedList);

					break;

				}
				}

				super.render(renderRequest, renderResponse);

				session.setAttribute("selectedOrgId", userOrgID, PortletSession.PORTLET_SCOPE);
				session.setAttribute("selectedPositionName", userPositionName, PortletSession.PORTLET_SCOPE);

			} catch (Exception e) {
				System.out.println("____________Exception from CompliancePortlet render method ");
				e.printStackTrace();
			}

		} else {
			System.out.println("_no user is logged in");
		}
	}

	/*
	 * gets selected intermediary position id on page submit to be used to fetch
	 * relevant proceses/function applicable for selected position
	 */
	@ProcessAction(name = "getIntermediaryPositionId")
	public void getIntermediaryPositionId(ActionRequest actionRequest, ActionResponse actionResponse) {

		PortletSession session = actionRequest.getPortletSession();
		session.setAttribute("selectedIntermediaryPositionId",
				ParamUtil.getLong(actionRequest, "oldPositionId", PortletSession.PORTLET_SCOPE));
	}

	/*
	 * invoked when contract number is selected carrying with it selected orgId &
	 * positionName, these are used during render for drill down to next level
	 */
	@ProcessAction(name = "drillDownHierachy")
	public void drillDownHierachy(ActionRequest actionRequest, ActionResponse actionResponse) {

		// update session attributes for orgId & positionName
		PortletSession session = actionRequest.getPortletSession();
		session.setAttribute("selectedOrgId", ParamUtil.getLong(actionRequest, "orgId"), PortletSession.PORTLET_SCOPE);
		session.setAttribute("selectedPositionName", ParamUtil.getString(actionRequest, "positionName"),
				PortletSession.PORTLET_SCOPE);

	}

	/* supply the parent val to child url */
	@ProcessAction(name = "getChildOrgs")
	public void getChildOrgs(ActionRequest actionRequest, ActionResponse actionResponse) {
		System.out.println("the action method called here: ");
		long parentOrgId = ParamUtil.getLong(actionRequest, "newParentOrgId");
		childOrganizationsList = ComplianceUtils.createChildOrganizations(parentOrgId);
		System.out.println("This is the size of the Child List: " + childOrganizationsList.size());

	}

	/* add compliance method - via interface */
	@ProcessAction(name = "addRecord")
	public void addRecord(ActionRequest actionRequest, ActionResponse response) throws PortalException {

		System.out.println("________________add recordis called_");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Compliance.class.getName(), actionRequest);

		long entityId = Long.parseLong(ParamUtil.getString(actionRequest, "entityId"));
		long contractId = Long.parseLong(ParamUtil.getString(actionRequest, "contractId"));
		String contractNumber = ParamUtil.getString(actionRequest, "contractNumber");
		String oldOrgId = ParamUtil.getString(actionRequest, "oldOrgId");
		String oldPositionId = ParamUtil.getString(actionRequest, "oldPositionId");

		String newOrgName1 = ParamUtil.getString(actionRequest, "newOrgName1");
		String newPositionName1 = ParamUtil.getString(actionRequest, "newPositionName1");

		String newOrgName2 = ParamUtil.getString(actionRequest, "newOrgName2");
		String newPositionName2 = ParamUtil.getString(actionRequest, "newPositionName2");

		/*
		 * String approverCategory = ParamUtil.getString(actionRequest,
		 * "applicationCategory"); String approverAction =
		 * ParamUtil.getString(actionRequest, "approverAction");
		 */
		
		String approverCategory = ParamUtil.getString(actionRequest, "approverAction");
		String approverAction = ParamUtil.getString(actionRequest, "applicationCategory");

		System.out.println(
				"++++++++___________________________________________________approverCategory : " + approverCategory);
		System.out.println(
				"++++++++___________________________________________________approverAction   : " + approverAction);

		String comments = ParamUtil.getString(actionRequest, "comments");

		String firstName = ParamUtil.getString(actionRequest, "firstName");
		String middleName = ParamUtil.getString(actionRequest, "middleName");
		String lastName = ParamUtil.getString(actionRequest, "lastName");
		String approverContractNumber = ParamUtil.getString(actionRequest, "approverContractNumber");
		String capacity = ParamUtil.getString(actionRequest, "capacity");

		String historyStartDate = ParamUtil.getString(actionRequest, "historyStartDate");
		String historyEndDate = ParamUtil.getString(actionRequest, "historyEndDate");
		String contractStartDate = ParamUtil.getString(actionRequest, "contractStartDate");
		String contractEndDate = ParamUtil.getString(actionRequest, "contractEndDate");

		String akiReasonCode = ParamUtil.getString(actionRequest, "akiReasonCode");

		boolean faLetter = ParamUtil.getBoolean(actionRequest, "faLetter");
		boolean fbmLetter = ParamUtil.getBoolean(actionRequest, "fbmLetter");
		boolean cbmLetter = ParamUtil.getBoolean(actionRequest, "cbmLetter");
		boolean iraLicence = ParamUtil.getBoolean(actionRequest, "iraLicence");

		String approvalLevel = "Level1";

		String assignmentStatus = ParamUtil.getString(actionRequest, "assignmentStatus");

		String newOrgId1 = null;
		String newOrgId2 = null;
		String newPositionId1 = null;
		String newPositionId2 = null;

		// takes care of FA promotion which only has one organization
		if ((ParamUtil.getString(actionRequest, "applicationCategory").equalsIgnoreCase("Promotion")
				|| ParamUtil.getString(actionRequest, "applicationCategory").equalsIgnoreCase("Transfer"))
				&& ParamUtil.getString(actionRequest, "oldPositionId").equalsIgnoreCase("1")) {
			newOrgId1 = ParamUtil.getString(actionRequest, "newOrgId2");
			newPositionId1 = ParamUtil.getString(actionRequest, "newPositionId2");
		} else {

			newOrgId1 = ParamUtil.getString(actionRequest, "newOrgId1");
			newOrgId2 = ParamUtil.getString(actionRequest, "newOrgId2");

			newPositionId1 = ParamUtil.getString(actionRequest, "newPositionId1");
			newPositionId2 = ParamUtil.getString(actionRequest, "newPositionId2");
		}

		String contractStatus = ParamUtil.getString(actionRequest, "contractStatus");

		long complianceId = ParamUtil.getLong(actionRequest, "complianceId");

		if (complianceId > 0) {
			System.out.println("________________ service updateCompliance is called_");

			try {

				complianceLocalService.updateCompliance(serviceContext.getUserId(), complianceId, entityId, contractId,
						contractNumber, oldOrgId, oldPositionId, newOrgName1, newPositionName1, newOrgName2,
						newPositionName2, approverCategory, approverAction, comments, firstName, middleName, lastName,
						approverContractNumber, capacity, historyStartDate, historyEndDate, contractStartDate,
						contractEndDate, akiReasonCode, faLetter, fbmLetter, cbmLetter, iraLicence, approvalLevel,
						assignmentStatus, newOrgId1, newPositionId1, newOrgId2, newPositionId2, contractStatus,
						serviceContext);

				// SessionMessages.add(actionRequest, "complianceAdded");
				// response.setRenderParameter("guestbookId", Long.toString(guestbookId));

			} catch (Exception e) {
				System.out.println(e);

				// SessionErrors.add(request, e.getClass().getName());
				// PortalUtil.copyRequestParameters(request, response);
				// actionResponse.setRenderParameter("mvcPath", "/more-info.jsp");
			}

		} else {
			System.out.println("________________ service addCompliance is called_");
			System.out.println("________________ serviceContext.getUserId()_: " + serviceContext.getUserId());

			try {
				complianceLocalService.addCompliance(serviceContext.getUserId(), entityId, contractId, contractNumber,
						oldOrgId, oldPositionId, newOrgName1, newPositionName1, newOrgName2, newPositionName2,
						approverCategory, approverAction, comments, firstName, middleName, lastName,
						approverContractNumber, capacity, historyStartDate, historyEndDate, contractStartDate,
						contractEndDate, akiReasonCode, faLetter, fbmLetter, cbmLetter, iraLicence, approvalLevel,
						assignmentStatus, newOrgId1, newPositionId1, newOrgId2, newPositionId2, contractStatus,
						serviceContext);

				// SessionMessages.add(actionRequest, "complianceAdded");
				// response.setRenderParameter("guestbookId", Long.toString(complianceId));

			} catch (Exception e) {
				System.out.println(e);
				// SessionErrors.add(actionRequest, e.getClass().getName());
				// PortalUtil.copyRequestParameters(request, response);
				// response.setRenderParameter("mvcPath",
				// "/guestbookwebportlet/edit_compliance.jsp");
			}
		}
	}

	/* method to update Compliance */
	/* update compliance record during approval */
	@ProcessAction(name = "updateCompliance")
	public void updateCompliance(ActionRequest actionRequest, ActionResponse actionResponse) {

		String approverVerdict = ParamUtil.getString(actionRequest, "approverVerdict", GetterUtil.DEFAULT_STRING); // Approved

		Compliance compliance = null;
		try {
			compliance = complianceLocalService
					.getCompliance(ParamUtil.getLong(actionRequest, "complianceId", GetterUtil.DEFAULT_LONG));
		} catch (Exception e) {
			log.error(e.getCause(), e);
		}

		if (Validator.isNotNull(compliance)) {

			compliance.setApprovalDate(new Date());
			compliance.setApproverVerdict(approverVerdict);
			// compliance.setComments(ParamUtil.getString(actionRequest, "comments",
			// GetterUtil.DEFAULT_STRING));

			if (approverVerdict.equals("Accepted") || approverVerdict.equals("Rejected")) {
				compliance.setFinalStatus(true);
				compliance.setApprovalLevel("Completed");
			} else {
				compliance.setApprovalLevel(approverVerdict);
			}
			compliance.setApproverContractNumber(
					ParamUtil.getString(actionRequest, "approverContractNumber", GetterUtil.DEFAULT_STRING)); // hardcode

			compliance.setApproverComments(
					ParamUtil.getString(actionRequest, "approverComment", GetterUtil.DEFAULT_STRING));

			complianceLocalService.updateCompliance(compliance);

			actionResponse.setRenderParameter("mvcPath", "/approvals.jsp");

		}
	}

	@ProcessAction(name = "updateComplianceUserComment")
	public void updateComplianceUserComment(ActionRequest actionRequest, ActionResponse actionResponse) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();

		String approverVerdict = ParamUtil.getString(actionRequest, "approverVerdict", GetterUtil.DEFAULT_STRING); // Approved

		Compliance compliance = null;

		String contractNumber = "";
		String fullName = "";

		try {
			compliance = complianceLocalService
					.getCompliance(ParamUtil.getLong(actionRequest, "complianceId", GetterUtil.DEFAULT_LONG));
		} catch (Exception e) {
			log.error(e.getCause(), e);
		}

		if (Validator.isNotNull(compliance)) {

			compliance.setApproverVerdict(approverVerdict);
			compliance.setApproverContractNumber(contractNumber);
			complianceLocalService.updateCompliance(compliance);

			long commentId = counterLocalService.increment(UserComment.class.getName());

			UserComment userComment = userCommentLocalService.createUserComment(commentId);
			userComment.setComplianceId(ParamUtil.getLong(actionRequest, "complianceId", GetterUtil.DEFAULT_LONG));
			userComment.setCommentId(commentId);
			userComment.setComment(ParamUtil.getString(actionRequest, "approverComment", GetterUtil.DEFAULT_STRING));
			userComment.setApproverContractNumber(contractNumber);
			userComment.setApproverName(fullName);
			userCommentLocalService.addUserComment(userComment);

		}

		actionResponse.setRenderParameter("mvcPath", "/more-info.jsp");
	}

	@ProcessAction(name = "getComplianceId")
	public void getComplianceId(ActionRequest actionRequest, ActionResponse actionResponse) {

		PortletSession session = actionRequest.getPortletSession();

		session.setAttribute("newSelectedComplianceId",
				(long) ParamUtil.getLong(actionRequest, "complianceId", PortletSession.PORTLET_SCOPE),
				PortletSession.PORTLET_SCOPE);

	}

	/* add the new record in DB, invoked by add-record action in view */
	@ProcessAction(name = "addRecordOLD")
	public void addRecordOLD(ActionRequest actionRequest, ActionResponse actionResponse) throws PortalException {

		long complianceId = counterLocalService.increment(Compliance.class.getName());

		Compliance compliance = complianceLocalService.createCompliance(complianceId);
		compliance.setComplianceId(complianceId);

		compliance.setEntityId(Long.parseLong(ParamUtil.getString(actionRequest, "entityId")));
		compliance.setContractId(Long.parseLong(ParamUtil.getString(actionRequest, "contractId")));
		compliance.setContractNumber(ParamUtil.getString(actionRequest, "contractNumber"));
		compliance.setOldOrgId(ParamUtil.getString(actionRequest, "oldOrgId"));
		compliance.setOldPositionId(ParamUtil.getString(actionRequest, "oldPositionId"));

		compliance.setNewOrgName1(ParamUtil.getString(actionRequest, "newOrgName1"));
		compliance.setNewPositionName1(ParamUtil.getString(actionRequest, "newPositionName1"));
		compliance.setNewOrgName2(ParamUtil.getString(actionRequest, "newOrgName2"));
		compliance.setNewPositionName2(ParamUtil.getString(actionRequest, "newPositionName2"));
		compliance.setApproverCategory(ParamUtil.getString(actionRequest, "applicationCategory"));

		compliance.setApproverAction(ParamUtil.getString(actionRequest, "applicationCategory"));
		compliance.setComments(ParamUtil.getString(actionRequest, "comments"));
		compliance.setFirstName(ParamUtil.getString(actionRequest, "firstName"));
		compliance.setMiddleName(ParamUtil.getString(actionRequest, "middleName"));
		compliance.setLastName(ParamUtil.getString(actionRequest, "lastName"));

		compliance.setApproverContractNumber(ParamUtil.getString(actionRequest, "approverContractNumber"));
		compliance.setCapacity(ParamUtil.getString(actionRequest, "capacity"));
		compliance.setHistoryStartDate(ParamUtil.getString(actionRequest, "historyStartDate"));
		compliance.setHistoryEndDate(ParamUtil.getString(actionRequest, "historyEndDate"));
		compliance.setContractStartDate(ParamUtil.getString(actionRequest, "contractStartDate"));

		compliance.setContractEndDate(ParamUtil.getString(actionRequest, "contractEndDate"));
		compliance.setAkiReasonCode(ParamUtil.getString(actionRequest, "akiReasonCode"));
		compliance.setFaLetter(ParamUtil.getBoolean(actionRequest, "faLetter"));
		compliance.setFbmLetter(ParamUtil.getBoolean(actionRequest, "fbmLetter"));
		compliance.setCbmLetter(ParamUtil.getBoolean(actionRequest, "cbmLetter"));

		compliance.setIraLicence(ParamUtil.getBoolean(actionRequest, "iraLicence"));
		compliance.setApprovalLevel("Level1");
		compliance.setAssignmentStatus(ParamUtil.getString(actionRequest, "assignmentStatus"));

		// takes care of FA promotion which only has one organization
		if ((ParamUtil.getString(actionRequest, "applicationCategory").equalsIgnoreCase("Promotion")
				|| ParamUtil.getString(actionRequest, "applicationCategory").equalsIgnoreCase("Transfer"))
				&& ParamUtil.getString(actionRequest, "oldPositionId").equalsIgnoreCase("1")) {
			compliance.setNewOrgId1(ParamUtil.getString(actionRequest, "newOrgId2"));
			compliance.setNewPositionId1(ParamUtil.getString(actionRequest, "newPositionId2"));
		} else {

			compliance.setNewOrgId1(ParamUtil.getString(actionRequest, "newOrgId1"));
			compliance.setNewOrgId2(ParamUtil.getString(actionRequest, "newOrgId2"));

			compliance.setNewPositionId1(ParamUtil.getString(actionRequest, "newPositionId1"));
			compliance.setNewPositionId2(ParamUtil.getString(actionRequest, "newPositionId2"));
		}

		// compliance.setNewOrgId1(ParamUtil.getString(actionRequest, "newOrgId1"));
		// compliance.setNewPositionId1(ParamUtil.getString(actionRequest,
		// "newPositionId1"));

		// compliance.setNewOrgId2(ParamUtil.getString(actionRequest, "newOrgId2"));
		// compliance.setNewPositionId2(ParamUtil.getString(actionRequest,
		// "newPositionId2"));

		compliance.setContractStatus(ParamUtil.getString(actionRequest, "contractStatus"));

		complianceLocalService.addCompliance(compliance);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		String resourceID = GetterUtil.getString(resourceRequest.getResourceID());
		if (resourceID.equals("rmTransferResource")) {
			String CMD = resourceRequest.getParameter("CMD");
			JSONArray jsonResults = JSONFactoryUtil.createJSONArray();
			JSONObject obj;

			/* Get List of Test by ExampleId */
			if (CMD.equalsIgnoreCase("rmTrans")) {
				String example = resourceRequest.getParameter("rmTransOrgId1");
				// System.out.println("rmTransferResource"+example);

				long rmTransOrgId1 = Long.parseLong(example);
				System.out.println("rmTransferResource" + rmTransOrgId1);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + rmTransOrgId1);
					// System.out.println("This is org id inside tycatch" + rmTransOrgId1);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					// System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("rmTransList", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (CMD.equalsIgnoreCase("rmTranss")) {
				String example = resourceRequest.getParameter("rmTransOrgId2");
				// System.out.println("rmTransferResource"+example);

				long rmTransOrgId2 = Long.parseLong(example);
				System.out.println("rmTransferResource Part Two" + rmTransOrgId2);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + rmTransOrgId2);
					// System.out.println("This is org id inside tycatch" + rmTransOrgId2);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					// System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("rmTransListt", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("####rmTransferResource####&&&&&&&&&&&&&&&&&#####AJAX CALL####################");
		} else if (resourceID.equals("bmTransferResource")) {
			String CMD = resourceRequest.getParameter("CMD");
			JSONArray jsonResults = JSONFactoryUtil.createJSONArray();
			JSONObject obj;

			/* Get List of Test by ExampleId */
			if (CMD.equalsIgnoreCase("bmTrans")) {
				String example = resourceRequest.getParameter("bmTransOrgId1");
				// System.out.println("This is the section July bmTransOrgId1 14th");

				long bmTransOrgId1 = Long.parseLong(example);
				System.out.println("BmTransferResource" + bmTransOrgId1);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + bmTransOrgId1);
					System.out.println("This is org id inside tycatch" + bmTransOrgId1);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("bmTransList", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (CMD.equalsIgnoreCase("bmTranss")) {
				String example = resourceRequest.getParameter("bmTransOrgId2");
				// System.out.println("This is the section July bmTransOrgId2 14th");

				long bmTransOrgId2 = Long.parseLong(example);
				System.out.println("BmTransferResource" + bmTransOrgId2);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + bmTransOrgId2);
					System.out.println("This is org id inside tycatch" + bmTransOrgId2);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("bmTransListt", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("####bmTransferResource####&&&&&&&&&&&&&&&&&#####AJAX CALL####################");
		} else if (resourceID.equals("umTransferResource")) {
			String CMD = resourceRequest.getParameter("CMD");
			JSONArray jsonResults = JSONFactoryUtil.createJSONArray();
			JSONObject obj;

			/* Get List of Test by ExampleId */
			if (CMD.equalsIgnoreCase("umTrans")) {
				String example = resourceRequest.getParameter("umTransOrgId1");
				// System.out.println("This is the section July bmTransOrgId1 14th");

				long umTransOrgId1 = Long.parseLong(example);
				System.out.println("UmTransferResource" + umTransOrgId1);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + umTransOrgId1);
					System.out.println("This is org id inside tycatch" + umTransOrgId1);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("umTransList", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (CMD.equalsIgnoreCase("umTranss")) {
				String example = resourceRequest.getParameter("umTransOrgId2");
				// System.out.println("This is the section July bmTransOrgId1 14th");

				long umTransOrgId2 = Long.parseLong(example);
				System.out.println("UmTransferResource" + umTransOrgId2);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + umTransOrgId2);
					System.out.println("This is org id inside tycatch" + umTransOrgId2);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("umTransListt", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("####umTransferResource####&&&&&&&&&&&&&&&&&#####AJAX CALL####################");
		} else if (resourceID.equals("faTransferResource")) {
			String CMD = resourceRequest.getParameter("CMD");
			JSONArray jsonResults = JSONFactoryUtil.createJSONArray();
			JSONObject obj;

			/* Get List of Test by ExampleId */
			if (CMD.equalsIgnoreCase("faTrans")) {
				String example = resourceRequest.getParameter("faTransOrgId1");
				// System.out.println("This is the section July bmTransOrgId1 14th");

				long faTransOrgId1 = Long.parseLong(example);
				System.out.println("FaTransferResource" + faTransOrgId1);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + faTransOrgId1);
					System.out.println("This is org id inside tycatch" + faTransOrgId1);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("faTransList", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (CMD.equalsIgnoreCase("faTranss")) {
				String example = resourceRequest.getParameter("faTransOrgId2");
				// System.out.println("This is the section July bmTransOrgId1 14th");

				long faTransOrgId2 = Long.parseLong(example);
				System.out.println("faTransOrgId2" + faTransOrgId2);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + faTransOrgId2);
					System.out.println("This is org id inside tycatch" + faTransOrgId2);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("faTransListt", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("####faTransferResource####&&&&&&&&&&&&&&&&&#####AJAX CALL####################");
		} else if (resourceID.equals("rmPromoferResource")) {
			String CMD = resourceRequest.getParameter("CMD");
			JSONArray jsonResults = JSONFactoryUtil.createJSONArray();
			JSONObject obj;

			/* Get List of Test by ExampleId */
			if (CMD.equalsIgnoreCase("rmPromo")) {
				String example = resourceRequest.getParameter("rmPromoOrgId1");
				// System.out.println("rmPromoferResource"+example);

				long rmPromoOrgId1 = Long.parseLong(example);
				System.out.println("rmPromoferResource" + rmPromoOrgId1);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + rmPromoOrgId1);
					// System.out.println("This is org id inside tycatch" + rmPromoOrgId1);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					// System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("rmPromoList", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (CMD.equalsIgnoreCase("rmPromos")) {
				String example = resourceRequest.getParameter("rmPromoOrgId2");
				// System.out.println("rmPromoferResource"+example);

				long rmPromoOrgId2 = Long.parseLong(example);
				System.out.println("rmPromoferResource Part Two" + rmPromoOrgId2);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + rmPromoOrgId2);
					// System.out.println("This is org id inside tycatch" + rmPromoOrgId2);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					// System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("rmPromoListt", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("####rmPromoferResource####&&&&&&&&&&&&&&&&&#####AJAX CALL####################");
		} else if (resourceID.equals("bmPromoferResource")) {
			String CMD = resourceRequest.getParameter("CMD");
			JSONArray jsonResults = JSONFactoryUtil.createJSONArray();
			JSONObject obj;

			/* Get List of Test by ExampleId */
			if (CMD.equalsIgnoreCase("bmPromo")) {
				String example = resourceRequest.getParameter("bmPromoOrgId1");
				// System.out.println("This is the section July bmPromoOrgId1 14th");

				long bmPromoOrgId1 = Long.parseLong(example);
				System.out.println("BmPromoferResource" + bmPromoOrgId1);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + bmPromoOrgId1);
					System.out.println("This is org id inside tycatch" + bmPromoOrgId1);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("bmPromoList", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (CMD.equalsIgnoreCase("bmPromos")) {
				String example = resourceRequest.getParameter("bmPromoOrgId2");
				// System.out.println("This is the section July bmPromoOrgId2 14th");

				long bmPromoOrgId2 = Long.parseLong(example);
				System.out.println("BmPromoferResource" + bmPromoOrgId2);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + bmPromoOrgId2);
					System.out.println("This is org id inside tycatch" + bmPromoOrgId2);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("bmPromoListt", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("####bmPromoferResource####&&&&&&&&&&&&&&&&&#####AJAX CALL####################");
		} else if (resourceID.equals("umPromoferResource")) {
			String CMD = resourceRequest.getParameter("CMD");
			JSONArray jsonResults = JSONFactoryUtil.createJSONArray();
			JSONObject obj;

			/* Get List of Test by ExampleId */
			if (CMD.equalsIgnoreCase("umPromo")) {
				String example = resourceRequest.getParameter("umPromoOrgId1");
				// System.out.println("This is the section July bmPromoOrgId1 14th");

				long umPromoOrgId1 = Long.parseLong(example);
				System.out.println("UmPromoferResource" + umPromoOrgId1);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + umPromoOrgId1);
					System.out.println("This is org id inside tycatch" + umPromoOrgId1);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("umPromoList", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (CMD.equalsIgnoreCase("umPromos")) {
				String example = resourceRequest.getParameter("umPromoOrgId2");
				// System.out.println("This is the section July bmPromoOrgId1 14th");

				long umPromoOrgId2 = Long.parseLong(example);
				System.out.println("UmPromoferResource" + umPromoOrgId2);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + umPromoOrgId2);
					System.out.println("This is org id inside tycatch" + umPromoOrgId2);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("umPromoListt", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("####umPromoferResource####&&&&&&&&&&&&&&&&&#####AJAX CALL####################");
		} else if (resourceID.equals("faPromoferResource")) {
			String CMD = resourceRequest.getParameter("CMD");
			JSONArray jsonResults = JSONFactoryUtil.createJSONArray();
			JSONObject obj;

			/* Get List of Test by ExampleId */
			if (CMD.equalsIgnoreCase("faPromo")) {
				String example = resourceRequest.getParameter("faPromoOrgId1");
				// System.out.println("This is the section July bmPromoOrgId1 14th");

				long faPromoOrgId1 = Long.parseLong(example);
				System.out.println("FaPromoferResource" + faPromoOrgId1);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + faPromoOrgId1);
					System.out.println("This is org id inside tycatch" + faPromoOrgId1);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("faPromoList", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (CMD.equalsIgnoreCase("faPromos")) {
				String example = resourceRequest.getParameter("faPromoOrgId2");
				// System.out.println("This is the section July bmPromoOrgId1 14th");

				long faPromoOrgId2 = Long.parseLong(example);
				System.out.println("faPromoOrgId2" + faPromoOrgId2);
				List<Organizations> tests;
				try {
					tests = new ArrayList<>();
					/* This is the new section */
					StringBuffer responseContent = new StringBuffer();
					URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + faPromoOrgId2);
					System.out.println("This is org id inside tycatch" + faPromoOrgId2);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP Error coded : " + conn.getResponseCode());
					}
					InputStreamReader in = new InputStreamReader(conn.getInputStream());
					BufferedReader br = new BufferedReader(in);
					String output;
					while ((output = br.readLine()) != null) {

						responseContent.append(output);
					}
					br.close();
					conn.disconnect();

					// call method to create objects
					JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
					JSONArray fas = jObj.getJSONArray("items");
					for (int i = 0; i < fas.length(); i++) {
						JSONObject fa = fas.getJSONObject(i);

						Organizations organizations = new Organizations();
						organizations.setOrgId(fa.getLong("org_id"));
						organizations.setOrgName(fa.getString("org_name"));

						tests.add(organizations);
					}

					/* This is the end of the new section */

					// tests = TestLocalServiceUtil.getTestByExampleId(exampleId);

					System.out.println("Tests size" + tests.size());
					if (tests.size() > 0) {
						for (Organizations test : tests) {

							jsonResults.put(test.getOrgId() + ":" + test.getOrgName());
						}
					}
					obj = JSONFactoryUtil.createJSONObject();
					obj.put("faPromoListt", jsonResults.toString());
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write(obj.toString());
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("####faPromoferResource####&&&&&&&&&&&&&&&&&#####AJAX CALL####################");
		} else {
			super.serveResource(resourceRequest, resourceResponse);
			System.out.println("#####elsePromoferResource###&&&&&&&&&&&&&&&&&#####AJAX CALL####################");
		}
	}

	@Reference(unbind = "-")
	protected void setComplianceService(ComplianceLocalService complianceLocalService) {
		complianceLocalService = complianceLocalService;
	}

	@Reference(unbind = "-")
	protected void setCounterService(CounterLocalService counterLocalService) {
		counterLocalService = counterLocalService;
	}

	public void setUserCommentLocalService(UserCommentLocalService userCommentLocalService) {
		this.userCommentLocalService = userCommentLocalService;
	}
}