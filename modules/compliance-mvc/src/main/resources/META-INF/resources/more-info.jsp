<%@ include file="init.jsp"%>
<%@page import="com.compliance.service.model.Compliance"%>
<%@page import="com.compliance.service.model.UserComment"%>
<%@page import="java.util.List"%>
<portlet:defineObjects />

<%
	List<Compliance> complianceList = (List<Compliance>) request.getAttribute("complianceList");
	List<Object[]> commentsList = (List<Object[]>) request.getAttribute("commentsList");
	List<UserComment> userCommentList = (List<UserComment>) request.getAttribute("userCommentList");
%>

<portlet:renderURL var="addComplianceRenderURL">
	<portlet:param name="mvcPath" value="/add-record.jsp" />
</portlet:renderURL>

<liferay-util:include page="/navigation_bar.jsp"
	servletContext="<%=application%>">
	<liferay-util:param name="searchEnabled" value="false" />
</liferay-util:include>

<h2>Requests on Hold</h2>
<table class="table table-striped">
	<tr>
		<th>Contract Number</th>
		<th>Name</th>
		<th>Request Date</th>
		<th>Category</th>
		<th colspan="2" style="width: 100px">Action</th>
	</tr>
	<c:forEach items="${complianceList}" var="compliance">

		<portlet:renderURL var="updateComplianceRenderURL">
			<portlet:param name="mvcPath" value="/more-info-update.jsp" />
			<portlet:param name="contractNumber"
				value="${compliance.contractNumber}" />
			<portlet:param name="firstName" value="${compliance.firstName}" />
			<portlet:param name="middleName" value="${compliance.middleName}" />
			<portlet:param name="lastName" value="${compliance.lastName}" />

			<portlet:param name="lastName" value="${compliance.approvalLevel}" />
			<portlet:param name="createDate" value="${compliance.createDate}" />
			<!-- <portlet:param name="comments" value="${compliance.comments}"/> -->
			<portlet:param name="approverContractNumber"
				value="${compliance.approverContractNumber}" />
			<portlet:param name="complianceId" value="${compliance.complianceId}" />
			<portlet:param name="approverCategory"
				value="${compliance.approverCategory}" />
			<portlet:param name="approverComments"
				value="${compliance.approverComments}" />
		</portlet:renderURL>

		<portlet:actionURL name="getComplianceId"
			var="getComplianceIdActionURL">
			<portlet:param name="complianceId"
				value="${compliance.getComplianceId()}" />
			<portlet:param name="mvcPath" value="/more-info-update.jsp" />
			<portlet:param name="contractNumber"
				value="${compliance.contractNumber}" />
			<portlet:param name="firstName" value="${compliance.firstName}" />
			<portlet:param name="middleName" value="${compliance.middleName}" />
			<portlet:param name="lastName" value="${compliance.lastName}" />
			<portlet:param name="lastName" value="${compliance.approvalLevel}" />
			<portlet:param name="createDate" value="${compliance.createDate}" />
			<!-- <portlet:param name="comments" value="${compliance.comments}"/> -->
			<portlet:param name="approverContractNumber"
				value="${compliance.approverContractNumber}" />
			<portlet:param name="complianceId" value="${compliance.complianceId}" />
			<portlet:param name="approverCategory"
				value="${compliance.approverCategory}" />
			<portlet:param name="approverComments"
				value="${compliance.approverComments}" />
		</portlet:actionURL>
		<!--  -->
		<tr>
			<td>${compliance.getContractNumber()}</td>
			<td>${compliance.getFirstName()}${compliance.getMiddleName()}
				${compliance.getLastName()}</td>

			<td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${compliance.getCreateDate()}" /></td>
			<td>${compliance.getApproverCategory()}</td>
			<!-- <td>${compliance.getCreateDate()}</td>
            <td>${compliance.getCreateDate()}</td> -->
			<%-- <td class="text-center" style="width: 50px"><a
				href="<%=updateComplianceRenderURL%>"
				class="btn  btn-primary btn-default btn-sm px-2 py-1"> <i
					class="glyphicon glyphicon-edit"></i> Respond
			</a></td> --%>
			<td class="text-center" style="width: 50px"><a
				href="<%=getComplianceIdActionURL%>"
				class="btn  btn-primary btn-default btn-sm px-2 py-1"> <i
					class="glyphicon glyphicon-edit"></i>Respond
			</a></td>
		</tr>
	</c:forEach>
</table>