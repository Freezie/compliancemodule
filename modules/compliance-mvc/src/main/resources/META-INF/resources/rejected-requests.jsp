<%@ include file="init.jsp"%>
<%@page import="com.compliance.service.model.Compliance"%>
<%@page import="java.util.List"%>
<portlet:defineObjects />

<% 
List<Compliance> requestRejectedList = (List<Compliance>)request.getAttribute("requestRejectedList");%>

<liferay-util:include page="/navigation_bar.jsp"
			servletContext="<%=application%>">
	<liferay-util:param name="searchEnabled" value="false" />
</liferay-util:include>
<h2>Rejected Request</h2>
<table class="table table-striped">
	<tr>
		<th>Contract Number</th>
		<th>Name</th>	
        <th>Category</th> 
        <th >Approver Verdict</th>
        <th colspan="2" style="width: 100px" >Request Date</th>  
	</tr>
	<c:forEach items="${requestRejectedList}" var="compliance">
		<tr>
			<td>${compliance.getContractNumber()}</td>
			<td>${compliance.getFirstName()} ${compliance.getMiddleName()} ${compliance.getLastName()}</td>
			<td>${compliance.getApproverCategory()}</td>
			<td>${compliance.getApproverVerdict()}</td>
			<td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${compliance.getCreateDate()}" /></td>			
		</tr>
	</c:forEach>
</table>