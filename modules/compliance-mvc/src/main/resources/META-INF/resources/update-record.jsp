<%@ include file="init.jsp"%>
<portlet:defineObjects />

<portlet:actionURL name="updateCompliance"
	var="updateComplianceActionURL" />

<aui:form action="<%=updateComplianceActionURL%>" name="complianceForm"
	method="POST" />

<%
	String complianceId = renderRequest.getParameter("complianceId");
	String contractNumber = renderRequest.getParameter("contractNumber");
	String entityId = renderRequest.getParameter("entityId");
	String comments = renderRequest.getParameter("comments");
	String approverContractNumber = renderRequest.getParameter("approverContractNumber");
	String approvalLevel = renderRequest.getParameter("approvalLevel");
	String approverComments = renderRequest.getParameter("approverComments");
	String contractId = renderRequest.getParameter("contractId");
	String positionId = renderRequest.getParameter("positionId");
	String orgId = renderRequest.getParameter("orgId");
	String firstName = renderRequest.getParameter("firstName");
	String middleName = renderRequest.getParameter("middleName");
	String lastName = renderRequest.getParameter("lastName");
	String newPositionName1 = renderRequest.getParameter("newPositionName1");
	String newOrgName1 = renderRequest.getParameter("newOrgName1");
	
	String newPositionName2 = renderRequest.getParameter("newPositionName2");
	String newOrgName2 = renderRequest.getParameter("newOrgName2");
	
	String spacer = " ";
%>

<liferay-util:include page="/navigation_bar.jsp"
			servletContext="<%=application%>">
	<liferay-util:param name="searchEnabled" value="false" />
</liferay-util:include>
<h2>Requests Approval</h2>
<aui:form action="<%=updateComplianceActionURL%>" method="post">

<label><b>Contract Number</b></label>
	<label><%=contractNumber%></label>
	<label> <b>Name</b></label>
	<label><%=firstName%><%=spacer%><%=middleName%><%=spacer%><%=lastName%></label>
	
	</br>
	
	<label><b>Organization</b></label>
	<label><%=newOrgName1%></label>
	<label><b>Position</b></label>
	<label><%=newPositionName1%></label>
	</br>
	</br>
	</br>
	
	<aui:input id="comments" name="comments" type="textarea" 
		value="${compliance.comments}" readonly="true" label="Past Comment"/>

	<aui:select name="approverVerdict" required="true"
		label="Select Option:">
		<aui:option value="">Approval Action</aui:option>
		<aui:option value="Accepted">Accepted</aui:option>
		<aui:option value="Rejected">Rejected</aui:option>
		<aui:option value="MoreInfo">Request More Information</aui:option>
	</aui:select>
	
	<aui:input id="comments2" name="approverComment" type="textarea" label="Approver Comment"/>

	<aui:button type="submit" name="" value="Submit Response"></aui:button>
	
	<aui:input name="complianceId" type="hidden" value="<%=complianceId%>" />
</aui:form>





