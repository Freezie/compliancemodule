/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.service.impl;

import com.compliance.service.exception.NoSuchComplianceException;
import com.compliance.service.model.Compliance;
import com.compliance.service.service.base.ComplianceLocalServiceBaseImpl;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetLinkConstants;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.workflow.WorkflowHandlerRegistryUtil;

import java.util.Date;
import java.util.List;

/**
 * The implementation of the compliance local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.compliance.service.service.ComplianceLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ComplianceLocalServiceBaseImpl
 */
public class ComplianceLocalServiceImpl extends ComplianceLocalServiceBaseImpl {

	public Compliance addCompliance(long userId, long entityId, long contractId, String contractNumber, String oldOrgId,
			String oldPositionId, String newOrgName1, String newPositionName1, String newOrgName2,
			String newPositionName2, String approverCategory, String approverAction, String comments, String firstName,
			String middleName, String lastName, String approverContractNumber, String capacity, String historyStartDate,
			String historyEndDate, String contractStartDate, String contractEndDate, String akiReasonCode,

			boolean faLetter, boolean fbmLetter, boolean cbmLetter, boolean iraLicence,

			String approvalLevel, String assignmentStatus, String newOrgId1, String newPositionId1, String newOrgId2,
			String newPositionId2, String contractStatus, ServiceContext serviceContext) throws PortalException {

		System.out.println("________________ service addCompliance in service layer is called_");

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		long complianceId = counterLocalService.increment();

		Compliance compliance = compliancePersistence.create(complianceId);

		compliance.setUuid(serviceContext.getUuid());
		compliance.setGroupId(groupId);
		compliance.setCompanyId(user.getCompanyId());
		compliance.setCreateDate(serviceContext.getCreateDate(now));
		compliance.setModifiedDate(serviceContext.getModifiedDate(now));
		compliance.setExpandoBridgeAttributes(serviceContext);

		compliance.setEntityId(entityId);
		compliance.setContractId(contractId);
		compliance.setContractNumber(contractNumber);
		compliance.setOldOrgId(oldOrgId);
		compliance.setOldPositionId(oldPositionId);
		compliance.setNewOrgName1(newOrgName1);
		compliance.setNewPositionName1(newPositionId1);
		compliance.setNewOrgName2(newOrgName2);
		compliance.setNewPositionName2(newPositionName2);
		compliance.setApproverCategory(approverCategory);
		compliance.setApproverAction(approverAction);
		compliance.setComments(comments);
		compliance.setFirstName(firstName);
		compliance.setMiddleName(middleName);
		compliance.setLastName(lastName);
		compliance.setApproverContractNumber(approverContractNumber);
		compliance.setCapacity(capacity);
		compliance.setHistoryStartDate(historyStartDate);
		compliance.setHistoryEndDate(historyEndDate);
		compliance.setContractStartDate(contractStartDate);
		compliance.setContractEndDate(contractEndDate);
		compliance.setAkiReasonCode(akiReasonCode);
		compliance.setFaLetter(faLetter);
		compliance.setFbmLetter(fbmLetter);
		compliance.setCbmLetter(cbmLetter);
		compliance.setIraLicence(iraLicence);
		compliance.setApprovalLevel(approvalLevel);
		compliance.setAssignmentStatus(assignmentStatus);
		compliance.setNewOrgId1(newOrgId1);
		compliance.setNewPositionId1(newPositionId1);
		compliance.setNewOrgId2(newOrgId2);
		compliance.setNewPositionId2(newPositionId2);
		compliance.setContractStatus(contractStatus);

		// workflow parameters
		compliance.setStatus(WorkflowConstants.STATUS_DRAFT);
		compliance.setStatusByUserId(userId);
		compliance.setStatusByUserName(user.getFullName());
		compliance.setStatusDate(serviceContext.getModifiedDate(null));

		compliance.setCreatedby(userId);

		Compliance comp = compliancePersistence.update(compliance);

		AssetEntry assetEntry = assetEntryLocalService.updateEntry(userId, groupId, compliance.getCreateDate(),
				compliance.getModifiedDate(), Compliance.class.getName(), complianceId, compliance.getUuid(), 0,
				serviceContext.getAssetCategoryIds(), serviceContext.getAssetTagNames(), true, true, null, null, null,
				null, ContentTypes.TEXT_HTML, comp.getApproverCategory(), null, null, null, null, 0, 0, null);

		assetLinkLocalService.updateLinks(userId, assetEntry.getEntryId(), serviceContext.getAssetLinkEntryIds(),
				AssetLinkConstants.TYPE_RELATED);

		WorkflowHandlerRegistryUtil.startWorkflowInstance(comp.getCompanyId(), comp.getGroupId(), comp.getCreatedby(),
				Compliance.class.getName(), comp.getPrimaryKey(), comp, serviceContext);

		return compliance;
	}

	public Compliance updateCompliance(long userId, long complianceId, long entityId, long contractId,
			String contractNumber, String oldOrgId, String oldPositionId,

			String newOrgName1, String newPositionName1,

			String newOrgName2, String newPositionName2,

			String approverCategory, String approverAction, String comments, String firstName, String middleName,
			String lastName, String approverContractNumber, String capacity,

			String historyStartDate, String historyEndDate, String contractStartDate, String contractEndDate,

			String akiReasonCode, boolean faLetter, boolean fbmLetter, boolean cbmLetter, boolean iraLicence,

			String approvalLevel,

			String assignmentStatus,

			String newOrgId1, String newPositionId1,

			String newOrgId2, String newPositionId2,

			String contractStatus, ServiceContext serviceContext) throws PortalException, SystemException {

		Date now = new Date();

		Compliance compliance = getCompliance(complianceId);

		// User user = userLocalService.getUserById(userId);

		compliance.setModifiedDate(serviceContext.getModifiedDate(now));
		compliance.setExpandoBridgeAttributes(serviceContext);

		compliance.setEntityId(entityId);
		compliance.setContractId(contractId);
		compliance.setContractNumber(contractNumber);
		compliance.setOldOrgId(oldOrgId);
		compliance.setOldPositionId(oldPositionId);
		compliance.setNewOrgName1(newOrgName1);
		compliance.setNewPositionName1(newPositionId1);
		compliance.setNewOrgName2(newOrgName2);
		compliance.setNewPositionName2(newPositionName2);
		compliance.setApproverCategory(approverCategory);
		compliance.setApproverAction(approverAction);
		compliance.setComments(comments);
		compliance.setFirstName(firstName);
		compliance.setMiddleName(middleName);
		compliance.setLastName(lastName);
		compliance.setApproverContractNumber(approverContractNumber);
		compliance.setCapacity(capacity);
		compliance.setHistoryStartDate(historyStartDate);
		compliance.setHistoryEndDate(historyEndDate);
		compliance.setContractStartDate(contractStartDate);
		compliance.setContractEndDate(contractEndDate);
		compliance.setAkiReasonCode(akiReasonCode);
		compliance.setFaLetter(faLetter);
		compliance.setFbmLetter(fbmLetter);
		compliance.setCbmLetter(cbmLetter);
		compliance.setIraLicence(iraLicence);
		compliance.setApprovalLevel(approvalLevel);
		compliance.setAssignmentStatus(assignmentStatus);
		compliance.setNewOrgId1(newOrgId1);
		compliance.setNewPositionId1(newPositionId1);
		compliance.setNewOrgId2(newOrgId2);
		compliance.setNewPositionId2(newPositionId2);
		compliance.setContractStatus(contractStatus);

		compliancePersistence.update(compliance);

		AssetEntry assetEntry = assetEntryLocalService.updateEntry(compliance.getCreatedby(), compliance.getGroupId(),
				compliance.getCreateDate(), compliance.getModifiedDate(), Compliance.class.getName(), complianceId,
				compliance.getUuid(), 0, serviceContext.getAssetCategoryIds(), serviceContext.getAssetTagNames(), true,
				true, compliance.getCreateDate(), null, null, null, ContentTypes.TEXT_HTML,
				compliance.getContractNumber(), null, null, null, null, 0, 0, serviceContext.getAssetPriority());

		assetLinkLocalService.updateLinks(serviceContext.getUserId(), assetEntry.getEntryId(),
				serviceContext.getAssetLinkEntryIds(), AssetLinkConstants.TYPE_RELATED);

		return compliance;
	}

	public Compliance deleteCompliance(long complianceId, ServiceContext serviceContext) throws PortalException {

		Compliance compliance = getCompliance(complianceId);

		compliance = deleteCompliance(complianceId);

		AssetEntry assetEntry = assetEntryLocalService.fetchEntry(Compliance.class.getName(), complianceId);

		assetLinkLocalService.deleteLinks(assetEntry.getEntryId());

		assetEntryLocalService.deleteEntry(assetEntry);

		return compliance;
	}

	/* for workflow */
	public Compliance updateStatus(long userId, long complianceId, int status, ServiceContext serviceContext)
			throws PortalException, SystemException {

		User user = userLocalService.getUser(userId);
		Compliance compliance = getCompliance(complianceId);

		compliance.setStatus(status);
		compliance.setStatusByUserId(userId);
		compliance.setStatusByUserName(user.getFullName());
		compliance.setStatusDate(new Date());

		compliancePersistence.update(compliance);
		if (status == WorkflowConstants.STATUS_APPROVED) {

			assetEntryLocalService.updateVisible(Compliance.class.getName(), complianceId, true);

		} else {

			assetEntryLocalService.updateVisible(Compliance.class.getName(), complianceId, false);
		}

		return compliance;
	}

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.compliance.service.service.ComplianceLocalService</code> via
	 * injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.compliance.service.service.ComplianceLocalServiceUtil</code>.
	 */

	/**
	 * Returns all the compliances where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @return the matching compliances
	 */
	public List<Compliance> findByApprovalLevel(String approvalLevel) {
		return getCompliancePersistence().findByApprovalLevel(approvalLevel);
	}

	/**
	 * Returns all the compliances where finalStatus = &#63;.
	 *
	 * @param finalStatus the final status
	 * @return the matching compliances
	 */
	public List<Compliance> findByUnprocessedFinalStatus(boolean finalStatus, String processedFlag) {
		return getCompliancePersistence().findByUnprocessedFinalStatus(finalStatus, processedFlag);
	}

	/**
	 * Returns all the compliances where finalStatus = &#63;.
	 *
	 * @param finalStatus the final status
	 * @return the matching compliances
	 */
	public List<Compliance> findByFinalStatus(String processedFlag) {
		return null;
	}

	/**
	 * Returns all the compliances where approvalLevel = &#63; and finalStatus =
	 * &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus   the final status
	 * @return the matching compliances
	 */
	public List<Compliance> findByFinalApproved(String approvalLevel, boolean finalStatus) {

		return getCompliancePersistence().findByFinalApproved(approvalLevel, finalStatus);
	}

	/**
	 * Returns the compliance where complianceId = &#63; or throws a
	 * <code>NoSuchComplianceException</code> if it could not be found.
	 *
	 * @param complianceId the compliance ID
	 * @return the matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public Compliance findByApproverComments(long complianceId)
			throws com.compliance.service.exception.NoSuchComplianceException {

		return getCompliancePersistence().findByApproverComments(complianceId);
	}

	/**
	 * Returns all the compliances where approverVerdict = &#63; and processedFlag =
	 * &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag   the processed flag
	 * @return the matching compliances
	 */
	public List<Compliance> findByRequestApprovals(String approverVerdict, String processedFlag) {
		return getCompliancePersistence().findByRequestApprovals(approverVerdict, processedFlag);
	}

	/**
	 * Returns all the compliances where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status  the status
	 * @return the matching compliances
	 */
	public java.util.List<Compliance> findBystatus(long groupId, int status) {
		return getCompliancePersistence().findByG_S(groupId, status);
	}

	/**
	 * Returns a range of all the compliances where groupId = &#63; and status =
	 * &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code>
	 * instances. <code>start</code> and <code>end</code> are not primary keys, they
	 * are indexes in the result set. Thus, <code>0</code> refers to the first
	 * result in the set. Setting both <code>start</code> and <code>end</code> to
	 * <code>QueryUtil#ALL_POS</code> will return the full result set. If
	 * <code>orderByComparator</code> is specified, then the query will include the
	 * given ORDER BY logic. If <code>orderByComparator</code> is absent and
	 * pagination is required (<code>start</code> and <code>end</code> are not
	 * <code>QueryUtil#ALL_POS</code>), then the query will include the default
	 * ORDER BY logic from <code>ComplianceModelImpl</code>. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned result
	 * set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status  the status
	 * @param start   the lower bound of the range of compliances
	 * @param end     the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	public java.util.List<Compliance> findBystatus(long groupId, int status, int start, int end) {
		return getCompliancePersistence().findByG_S(groupId, status, start, end);
	}
}