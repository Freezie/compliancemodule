/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.model.impl;

import com.compliance.service.model.UserComment;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UserComment in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class UserCommentCacheModel
	implements CacheModel<UserComment>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserCommentCacheModel)) {
			return false;
		}

		UserCommentCacheModel userCommentCacheModel =
			(UserCommentCacheModel)obj;

		if (commentId == userCommentCacheModel.commentId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, commentId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", commentId=");
		sb.append(commentId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", createdby=");
		sb.append(createdby);
		sb.append(", modifiedby=");
		sb.append(modifiedby);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", commentDate=");
		sb.append(commentDate);
		sb.append(", approverName=");
		sb.append(approverName);
		sb.append(", approverContractNumber=");
		sb.append(approverContractNumber);
		sb.append(", comment=");
		sb.append(comment);
		sb.append(", complianceId=");
		sb.append(complianceId);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserComment toEntityModel() {
		UserCommentImpl userCommentImpl = new UserCommentImpl();

		if (uuid == null) {
			userCommentImpl.setUuid("");
		}
		else {
			userCommentImpl.setUuid(uuid);
		}

		userCommentImpl.setCommentId(commentId);
		userCommentImpl.setGroupId(groupId);
		userCommentImpl.setCompanyId(companyId);
		userCommentImpl.setCreatedby(createdby);

		if (modifiedby == null) {
			userCommentImpl.setModifiedby("");
		}
		else {
			userCommentImpl.setModifiedby(modifiedby);
		}

		if (createDate == Long.MIN_VALUE) {
			userCommentImpl.setCreateDate(null);
		}
		else {
			userCommentImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			userCommentImpl.setModifiedDate(null);
		}
		else {
			userCommentImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (commentDate == Long.MIN_VALUE) {
			userCommentImpl.setCommentDate(null);
		}
		else {
			userCommentImpl.setCommentDate(new Date(commentDate));
		}

		if (approverName == null) {
			userCommentImpl.setApproverName("");
		}
		else {
			userCommentImpl.setApproverName(approverName);
		}

		if (approverContractNumber == null) {
			userCommentImpl.setApproverContractNumber("");
		}
		else {
			userCommentImpl.setApproverContractNumber(approverContractNumber);
		}

		if (comment == null) {
			userCommentImpl.setComment("");
		}
		else {
			userCommentImpl.setComment(comment);
		}

		userCommentImpl.setComplianceId(complianceId);
		userCommentImpl.setStatus(status);
		userCommentImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			userCommentImpl.setStatusByUserName("");
		}
		else {
			userCommentImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			userCommentImpl.setStatusDate(null);
		}
		else {
			userCommentImpl.setStatusDate(new Date(statusDate));
		}

		userCommentImpl.resetOriginalValues();

		return userCommentImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		commentId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		createdby = objectInput.readLong();
		modifiedby = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		commentDate = objectInput.readLong();
		approverName = objectInput.readUTF();
		approverContractNumber = objectInput.readUTF();
		comment = objectInput.readUTF();

		complianceId = objectInput.readLong();

		status = objectInput.readInt();

		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(commentId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(createdby);

		if (modifiedby == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(modifiedby);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(commentDate);

		if (approverName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(approverName);
		}

		if (approverContractNumber == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(approverContractNumber);
		}

		if (comment == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(comment);
		}

		objectOutput.writeLong(complianceId);

		objectOutput.writeInt(status);

		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);
	}

	public String uuid;
	public long commentId;
	public long groupId;
	public long companyId;
	public long createdby;
	public String modifiedby;
	public long createDate;
	public long modifiedDate;
	public long commentDate;
	public String approverName;
	public String approverContractNumber;
	public String comment;
	public long complianceId;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;

}