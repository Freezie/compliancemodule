/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.model.impl;

import com.compliance.service.model.Compliance;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Compliance in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ComplianceCacheModel
	implements CacheModel<Compliance>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ComplianceCacheModel)) {
			return false;
		}

		ComplianceCacheModel complianceCacheModel = (ComplianceCacheModel)obj;

		if (complianceId == complianceCacheModel.complianceId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, complianceId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(105);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", complianceId=");
		sb.append(complianceId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", createdby=");
		sb.append(createdby);
		sb.append(", modifiedby=");
		sb.append(modifiedby);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", contractId=");
		sb.append(contractId);
		sb.append(", entityId=");
		sb.append(entityId);
		sb.append(", approvalDate=");
		sb.append(approvalDate);
		sb.append(", comments=");
		sb.append(comments);
		sb.append(", approverComments=");
		sb.append(approverComments);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", middleName=");
		sb.append(middleName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", contractNumber=");
		sb.append(contractNumber);
		sb.append(", approverContractNumber=");
		sb.append(approverContractNumber);
		sb.append(", approverVerdict=");
		sb.append(approverVerdict);
		sb.append(", approverAction=");
		sb.append(approverAction);
		sb.append(", errorReason=");
		sb.append(errorReason);
		sb.append(", assignmentStatus=");
		sb.append(assignmentStatus);
		sb.append(", capacity=");
		sb.append(capacity);
		sb.append(", newPositionId1=");
		sb.append(newPositionId1);
		sb.append(", newPositionName1=");
		sb.append(newPositionName1);
		sb.append(", newPositionId2=");
		sb.append(newPositionId2);
		sb.append(", newPositionName2=");
		sb.append(newPositionName2);
		sb.append(", newOrgId1=");
		sb.append(newOrgId1);
		sb.append(", newOrgName1=");
		sb.append(newOrgName1);
		sb.append(", newOrgId2=");
		sb.append(newOrgId2);
		sb.append(", newOrgName2=");
		sb.append(newOrgName2);
		sb.append(", historyStartDate=");
		sb.append(historyStartDate);
		sb.append(", historyEndDate=");
		sb.append(historyEndDate);
		sb.append(", oldOrgId=");
		sb.append(oldOrgId);
		sb.append(", oldPositionId=");
		sb.append(oldPositionId);
		sb.append(", contractStartDate=");
		sb.append(contractStartDate);
		sb.append(", contractEndDate=");
		sb.append(contractEndDate);
		sb.append(", contractStatus=");
		sb.append(contractStatus);
		sb.append(", approverCategory=");
		sb.append(approverCategory);
		sb.append(", approvalLevel=");
		sb.append(approvalLevel);
		sb.append(", akiReasonCode=");
		sb.append(akiReasonCode);
		sb.append(", processId=");
		sb.append(processId);
		sb.append(", faLetter=");
		sb.append(faLetter);
		sb.append(", fbmLetter=");
		sb.append(fbmLetter);
		sb.append(", cbmLetter=");
		sb.append(cbmLetter);
		sb.append(", iraLicence=");
		sb.append(iraLicence);
		sb.append(", finalStatus=");
		sb.append(finalStatus);
		sb.append(", processedFlag=");
		sb.append(processedFlag);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Compliance toEntityModel() {
		ComplianceImpl complianceImpl = new ComplianceImpl();

		if (uuid == null) {
			complianceImpl.setUuid("");
		}
		else {
			complianceImpl.setUuid(uuid);
		}

		complianceImpl.setComplianceId(complianceId);
		complianceImpl.setGroupId(groupId);
		complianceImpl.setCompanyId(companyId);
		complianceImpl.setCreatedby(createdby);

		if (modifiedby == null) {
			complianceImpl.setModifiedby("");
		}
		else {
			complianceImpl.setModifiedby(modifiedby);
		}

		if (createDate == Long.MIN_VALUE) {
			complianceImpl.setCreateDate(null);
		}
		else {
			complianceImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			complianceImpl.setModifiedDate(null);
		}
		else {
			complianceImpl.setModifiedDate(new Date(modifiedDate));
		}

		complianceImpl.setContractId(contractId);
		complianceImpl.setEntityId(entityId);

		if (approvalDate == Long.MIN_VALUE) {
			complianceImpl.setApprovalDate(null);
		}
		else {
			complianceImpl.setApprovalDate(new Date(approvalDate));
		}

		if (comments == null) {
			complianceImpl.setComments("");
		}
		else {
			complianceImpl.setComments(comments);
		}

		if (approverComments == null) {
			complianceImpl.setApproverComments("");
		}
		else {
			complianceImpl.setApproverComments(approverComments);
		}

		if (firstName == null) {
			complianceImpl.setFirstName("");
		}
		else {
			complianceImpl.setFirstName(firstName);
		}

		if (middleName == null) {
			complianceImpl.setMiddleName("");
		}
		else {
			complianceImpl.setMiddleName(middleName);
		}

		if (lastName == null) {
			complianceImpl.setLastName("");
		}
		else {
			complianceImpl.setLastName(lastName);
		}

		if (contractNumber == null) {
			complianceImpl.setContractNumber("");
		}
		else {
			complianceImpl.setContractNumber(contractNumber);
		}

		if (approverContractNumber == null) {
			complianceImpl.setApproverContractNumber("");
		}
		else {
			complianceImpl.setApproverContractNumber(approverContractNumber);
		}

		if (approverVerdict == null) {
			complianceImpl.setApproverVerdict("");
		}
		else {
			complianceImpl.setApproverVerdict(approverVerdict);
		}

		if (approverAction == null) {
			complianceImpl.setApproverAction("");
		}
		else {
			complianceImpl.setApproverAction(approverAction);
		}

		if (errorReason == null) {
			complianceImpl.setErrorReason("");
		}
		else {
			complianceImpl.setErrorReason(errorReason);
		}

		if (assignmentStatus == null) {
			complianceImpl.setAssignmentStatus("");
		}
		else {
			complianceImpl.setAssignmentStatus(assignmentStatus);
		}

		if (capacity == null) {
			complianceImpl.setCapacity("");
		}
		else {
			complianceImpl.setCapacity(capacity);
		}

		if (newPositionId1 == null) {
			complianceImpl.setNewPositionId1("");
		}
		else {
			complianceImpl.setNewPositionId1(newPositionId1);
		}

		if (newPositionName1 == null) {
			complianceImpl.setNewPositionName1("");
		}
		else {
			complianceImpl.setNewPositionName1(newPositionName1);
		}

		if (newPositionId2 == null) {
			complianceImpl.setNewPositionId2("");
		}
		else {
			complianceImpl.setNewPositionId2(newPositionId2);
		}

		if (newPositionName2 == null) {
			complianceImpl.setNewPositionName2("");
		}
		else {
			complianceImpl.setNewPositionName2(newPositionName2);
		}

		if (newOrgId1 == null) {
			complianceImpl.setNewOrgId1("");
		}
		else {
			complianceImpl.setNewOrgId1(newOrgId1);
		}

		if (newOrgName1 == null) {
			complianceImpl.setNewOrgName1("");
		}
		else {
			complianceImpl.setNewOrgName1(newOrgName1);
		}

		if (newOrgId2 == null) {
			complianceImpl.setNewOrgId2("");
		}
		else {
			complianceImpl.setNewOrgId2(newOrgId2);
		}

		if (newOrgName2 == null) {
			complianceImpl.setNewOrgName2("");
		}
		else {
			complianceImpl.setNewOrgName2(newOrgName2);
		}

		if (historyStartDate == null) {
			complianceImpl.setHistoryStartDate("");
		}
		else {
			complianceImpl.setHistoryStartDate(historyStartDate);
		}

		if (historyEndDate == null) {
			complianceImpl.setHistoryEndDate("");
		}
		else {
			complianceImpl.setHistoryEndDate(historyEndDate);
		}

		if (oldOrgId == null) {
			complianceImpl.setOldOrgId("");
		}
		else {
			complianceImpl.setOldOrgId(oldOrgId);
		}

		if (oldPositionId == null) {
			complianceImpl.setOldPositionId("");
		}
		else {
			complianceImpl.setOldPositionId(oldPositionId);
		}

		if (contractStartDate == null) {
			complianceImpl.setContractStartDate("");
		}
		else {
			complianceImpl.setContractStartDate(contractStartDate);
		}

		if (contractEndDate == null) {
			complianceImpl.setContractEndDate("");
		}
		else {
			complianceImpl.setContractEndDate(contractEndDate);
		}

		if (contractStatus == null) {
			complianceImpl.setContractStatus("");
		}
		else {
			complianceImpl.setContractStatus(contractStatus);
		}

		if (approverCategory == null) {
			complianceImpl.setApproverCategory("");
		}
		else {
			complianceImpl.setApproverCategory(approverCategory);
		}

		if (approvalLevel == null) {
			complianceImpl.setApprovalLevel("");
		}
		else {
			complianceImpl.setApprovalLevel(approvalLevel);
		}

		if (akiReasonCode == null) {
			complianceImpl.setAkiReasonCode("");
		}
		else {
			complianceImpl.setAkiReasonCode(akiReasonCode);
		}

		if (processId == null) {
			complianceImpl.setProcessId("");
		}
		else {
			complianceImpl.setProcessId(processId);
		}

		complianceImpl.setFaLetter(faLetter);
		complianceImpl.setFbmLetter(fbmLetter);
		complianceImpl.setCbmLetter(cbmLetter);
		complianceImpl.setIraLicence(iraLicence);
		complianceImpl.setFinalStatus(finalStatus);

		if (processedFlag == null) {
			complianceImpl.setProcessedFlag("");
		}
		else {
			complianceImpl.setProcessedFlag(processedFlag);
		}

		complianceImpl.setStatus(status);
		complianceImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			complianceImpl.setStatusByUserName("");
		}
		else {
			complianceImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			complianceImpl.setStatusDate(null);
		}
		else {
			complianceImpl.setStatusDate(new Date(statusDate));
		}

		complianceImpl.resetOriginalValues();

		return complianceImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		complianceId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		createdby = objectInput.readLong();
		modifiedby = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		contractId = objectInput.readLong();

		entityId = objectInput.readLong();
		approvalDate = objectInput.readLong();
		comments = objectInput.readUTF();
		approverComments = objectInput.readUTF();
		firstName = objectInput.readUTF();
		middleName = objectInput.readUTF();
		lastName = objectInput.readUTF();
		contractNumber = objectInput.readUTF();
		approverContractNumber = objectInput.readUTF();
		approverVerdict = objectInput.readUTF();
		approverAction = objectInput.readUTF();
		errorReason = objectInput.readUTF();
		assignmentStatus = objectInput.readUTF();
		capacity = objectInput.readUTF();
		newPositionId1 = objectInput.readUTF();
		newPositionName1 = objectInput.readUTF();
		newPositionId2 = objectInput.readUTF();
		newPositionName2 = objectInput.readUTF();
		newOrgId1 = objectInput.readUTF();
		newOrgName1 = objectInput.readUTF();
		newOrgId2 = objectInput.readUTF();
		newOrgName2 = objectInput.readUTF();
		historyStartDate = objectInput.readUTF();
		historyEndDate = objectInput.readUTF();
		oldOrgId = objectInput.readUTF();
		oldPositionId = objectInput.readUTF();
		contractStartDate = objectInput.readUTF();
		contractEndDate = objectInput.readUTF();
		contractStatus = objectInput.readUTF();
		approverCategory = objectInput.readUTF();
		approvalLevel = objectInput.readUTF();
		akiReasonCode = objectInput.readUTF();
		processId = objectInput.readUTF();

		faLetter = objectInput.readBoolean();

		fbmLetter = objectInput.readBoolean();

		cbmLetter = objectInput.readBoolean();

		iraLicence = objectInput.readBoolean();

		finalStatus = objectInput.readBoolean();
		processedFlag = objectInput.readUTF();

		status = objectInput.readInt();

		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(complianceId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(createdby);

		if (modifiedby == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(modifiedby);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(contractId);

		objectOutput.writeLong(entityId);
		objectOutput.writeLong(approvalDate);

		if (comments == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(comments);
		}

		if (approverComments == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(approverComments);
		}

		if (firstName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(firstName);
		}

		if (middleName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(middleName);
		}

		if (lastName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(lastName);
		}

		if (contractNumber == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(contractNumber);
		}

		if (approverContractNumber == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(approverContractNumber);
		}

		if (approverVerdict == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(approverVerdict);
		}

		if (approverAction == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(approverAction);
		}

		if (errorReason == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(errorReason);
		}

		if (assignmentStatus == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(assignmentStatus);
		}

		if (capacity == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(capacity);
		}

		if (newPositionId1 == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(newPositionId1);
		}

		if (newPositionName1 == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(newPositionName1);
		}

		if (newPositionId2 == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(newPositionId2);
		}

		if (newPositionName2 == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(newPositionName2);
		}

		if (newOrgId1 == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(newOrgId1);
		}

		if (newOrgName1 == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(newOrgName1);
		}

		if (newOrgId2 == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(newOrgId2);
		}

		if (newOrgName2 == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(newOrgName2);
		}

		if (historyStartDate == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(historyStartDate);
		}

		if (historyEndDate == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(historyEndDate);
		}

		if (oldOrgId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(oldOrgId);
		}

		if (oldPositionId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(oldPositionId);
		}

		if (contractStartDate == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(contractStartDate);
		}

		if (contractEndDate == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(contractEndDate);
		}

		if (contractStatus == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(contractStatus);
		}

		if (approverCategory == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(approverCategory);
		}

		if (approvalLevel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(approvalLevel);
		}

		if (akiReasonCode == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(akiReasonCode);
		}

		if (processId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(processId);
		}

		objectOutput.writeBoolean(faLetter);

		objectOutput.writeBoolean(fbmLetter);

		objectOutput.writeBoolean(cbmLetter);

		objectOutput.writeBoolean(iraLicence);

		objectOutput.writeBoolean(finalStatus);

		if (processedFlag == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(processedFlag);
		}

		objectOutput.writeInt(status);

		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);
	}

	public String uuid;
	public long complianceId;
	public long groupId;
	public long companyId;
	public long createdby;
	public String modifiedby;
	public long createDate;
	public long modifiedDate;
	public long contractId;
	public long entityId;
	public long approvalDate;
	public String comments;
	public String approverComments;
	public String firstName;
	public String middleName;
	public String lastName;
	public String contractNumber;
	public String approverContractNumber;
	public String approverVerdict;
	public String approverAction;
	public String errorReason;
	public String assignmentStatus;
	public String capacity;
	public String newPositionId1;
	public String newPositionName1;
	public String newPositionId2;
	public String newPositionName2;
	public String newOrgId1;
	public String newOrgName1;
	public String newOrgId2;
	public String newOrgName2;
	public String historyStartDate;
	public String historyEndDate;
	public String oldOrgId;
	public String oldPositionId;
	public String contractStartDate;
	public String contractEndDate;
	public String contractStatus;
	public String approverCategory;
	public String approvalLevel;
	public String akiReasonCode;
	public String processId;
	public boolean faLetter;
	public boolean fbmLetter;
	public boolean cbmLetter;
	public boolean iraLicence;
	public boolean finalStatus;
	public String processedFlag;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;

}