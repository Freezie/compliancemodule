/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.service;

import aQute.bnd.annotation.ProviderType;

import com.compliance.service.exception.NoSuchComplianceException;
import com.compliance.service.model.Compliance;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for Compliance. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see ComplianceLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface ComplianceLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ComplianceLocalServiceUtil} to access the compliance local service. Add custom service methods to <code>com.compliance.service.service.impl.ComplianceLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	 * Adds the compliance to the database. Also notifies the appropriate model listeners.
	 *
	 * @param compliance the compliance
	 * @return the compliance that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Compliance addCompliance(Compliance compliance);

	public Compliance addCompliance(
			long userId, long entityId, long contractId, String contractNumber,
			String oldOrgId, String oldPositionId, String newOrgName1,
			String newPositionName1, String newOrgName2,
			String newPositionName2, String approverCategory,
			String approverAction, String comments, String firstName,
			String middleName, String lastName, String approverContractNumber,
			String capacity, String historyStartDate, String historyEndDate,
			String contractStartDate, String contractEndDate,
			String akiReasonCode, boolean faLetter, boolean fbmLetter,
			boolean cbmLetter, boolean iraLicence, String approvalLevel,
			String assignmentStatus, String newOrgId1, String newPositionId1,
			String newOrgId2, String newPositionId2, String contractStatus,
			ServiceContext serviceContext)
		throws PortalException;

	/**
	 * Creates a new compliance with the primary key. Does not add the compliance to the database.
	 *
	 * @param complianceId the primary key for the new compliance
	 * @return the new compliance
	 */
	@Transactional(enabled = false)
	public Compliance createCompliance(long complianceId);

	/**
	 * Deletes the compliance from the database. Also notifies the appropriate model listeners.
	 *
	 * @param compliance the compliance
	 * @return the compliance that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Compliance deleteCompliance(Compliance compliance);

	/**
	 * Deletes the compliance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param complianceId the primary key of the compliance
	 * @return the compliance that was removed
	 * @throws PortalException if a compliance with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Compliance deleteCompliance(long complianceId)
		throws PortalException;

	public Compliance deleteCompliance(
			long complianceId, ServiceContext serviceContext)
		throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.compliance.service.model.impl.ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.compliance.service.model.impl.ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Compliance fetchCompliance(long complianceId);

	/**
	 * Returns the compliance matching the UUID and group.
	 *
	 * @param uuid the compliance's UUID
	 * @param groupId the primary key of the group
	 * @return the matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Compliance fetchComplianceByUuidAndGroupId(
		String uuid, long groupId);

	/**
	 * Returns all the compliances where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @return the matching compliances
	 */
	public List<Compliance> findByApprovalLevel(String approvalLevel);

	/**
	 * Returns the compliance where complianceId = &#63; or throws a
	 * <code>NoSuchComplianceException</code> if it could not be found.
	 *
	 * @param complianceId the compliance ID
	 * @return the matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public Compliance findByApproverComments(long complianceId)
		throws NoSuchComplianceException;

	/**
	 * Returns all the compliances where approvalLevel = &#63; and finalStatus =
	 * &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus   the final status
	 * @return the matching compliances
	 */
	public List<Compliance> findByFinalApproved(
		String approvalLevel, boolean finalStatus);

	/**
	 * Returns all the compliances where finalStatus = &#63;.
	 *
	 * @param finalStatus the final status
	 * @return the matching compliances
	 */
	public List<Compliance> findByFinalStatus(String processedFlag);

	/**
	 * Returns all the compliances where approverVerdict = &#63; and processedFlag =
	 * &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag   the processed flag
	 * @return the matching compliances
	 */
	public List<Compliance> findByRequestApprovals(
		String approverVerdict, String processedFlag);

	/**
	 * Returns all the compliances where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status  the status
	 * @return the matching compliances
	 */
	public List<Compliance> findBystatus(long groupId, int status);

	/**
	 * Returns a range of all the compliances where groupId = &#63; and status =
	 * &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code>
	 * instances. <code>start</code> and <code>end</code> are not primary keys, they
	 * are indexes in the result set. Thus, <code>0</code> refers to the first
	 * result in the set. Setting both <code>start</code> and <code>end</code> to
	 * <code>QueryUtil#ALL_POS</code> will return the full result set. If
	 * <code>orderByComparator</code> is specified, then the query will include the
	 * given ORDER BY logic. If <code>orderByComparator</code> is absent and
	 * pagination is required (<code>start</code> and <code>end</code> are not
	 * <code>QueryUtil#ALL_POS</code>), then the query will include the default
	 * ORDER BY logic from <code>ComplianceModelImpl</code>. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned result
	 * set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status  the status
	 * @param start   the lower bound of the range of compliances
	 * @param end     the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	public List<Compliance> findBystatus(
		long groupId, int status, int start, int end);

	/**
	 * Returns all the compliances where finalStatus = &#63;.
	 *
	 * @param finalStatus the final status
	 * @return the matching compliances
	 */
	public List<Compliance> findByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns the compliance with the primary key.
	 *
	 * @param complianceId the primary key of the compliance
	 * @return the compliance
	 * @throws PortalException if a compliance with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Compliance getCompliance(long complianceId) throws PortalException;

	/**
	 * Returns the compliance matching the UUID and group.
	 *
	 * @param uuid the compliance's UUID
	 * @param groupId the primary key of the group
	 * @return the matching compliance
	 * @throws PortalException if a matching compliance could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Compliance getComplianceByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	/**
	 * Returns a range of all the compliances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.compliance.service.model.impl.ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of compliances
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Compliance> getCompliances(int start, int end);

	/**
	 * Returns all the compliances matching the UUID and company.
	 *
	 * @param uuid the UUID of the compliances
	 * @param companyId the primary key of the company
	 * @return the matching compliances, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Compliance> getCompliancesByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of compliances matching the UUID and company.
	 *
	 * @param uuid the UUID of the compliances
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching compliances, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Compliance> getCompliancesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Compliance> orderByComparator);

	/**
	 * Returns the number of compliances.
	 *
	 * @return the number of compliances
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getCompliancesCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Updates the compliance in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param compliance the compliance
	 * @return the compliance that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Compliance updateCompliance(Compliance compliance);

	public Compliance updateCompliance(
			long userId, long complianceId, long entityId, long contractId,
			String contractNumber, String oldOrgId, String oldPositionId,
			String newOrgName1, String newPositionName1, String newOrgName2,
			String newPositionName2, String approverCategory,
			String approverAction, String comments, String firstName,
			String middleName, String lastName, String approverContractNumber,
			String capacity, String historyStartDate, String historyEndDate,
			String contractStartDate, String contractEndDate,
			String akiReasonCode, boolean faLetter, boolean fbmLetter,
			boolean cbmLetter, boolean iraLicence, String approvalLevel,
			String assignmentStatus, String newOrgId1, String newPositionId1,
			String newOrgId2, String newPositionId2, String contractStatus,
			ServiceContext serviceContext)
		throws PortalException, SystemException;

	public Compliance updateStatus(
			long userId, long complianceId, int status,
			ServiceContext serviceContext)
		throws PortalException, SystemException;

}