/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.compliance.service.exception.NoSuchUserCommentException;
import com.compliance.service.model.UserComment;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import java.io.Serializable;

import java.util.Map;
import java.util.Set;

/**
 * The persistence interface for the user comment service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserCommentUtil
 * @generated
 */
@ProviderType
public interface UserCommentPersistence extends BasePersistence<UserComment> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserCommentUtil} to access the user comment persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */
	@Override
	public Map<Serializable, UserComment> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys);

	/**
	 * Returns all the user comments where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching user comments
	 */
	public java.util.List<UserComment> findByUuid(String uuid);

	/**
	 * Returns a range of all the user comments where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of matching user comments
	 */
	public java.util.List<UserComment> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the user comments where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user comments
	 */
	public java.util.List<UserComment> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns an ordered range of all the user comments where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user comments
	 */
	public java.util.List<UserComment> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first user comment in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public UserComment findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Returns the first user comment in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns the last user comment in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public UserComment findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Returns the last user comment in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns the user comments before and after the current user comment in the ordered set where uuid = &#63;.
	 *
	 * @param commentId the primary key of the current user comment
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public UserComment[] findByUuid_PrevAndNext(
			long commentId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Removes all the user comments where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of user comments where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching user comments
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the user comment where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchUserCommentException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public UserComment findByUUID_G(String uuid, long groupId)
		throws NoSuchUserCommentException;

	/**
	 * Returns the user comment where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the user comment where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the user comment where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the user comment that was removed
	 */
	public UserComment removeByUUID_G(String uuid, long groupId)
		throws NoSuchUserCommentException;

	/**
	 * Returns the number of user comments where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching user comments
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the user comments where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching user comments
	 */
	public java.util.List<UserComment> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the user comments where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of matching user comments
	 */
	public java.util.List<UserComment> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the user comments where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user comments
	 */
	public java.util.List<UserComment> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns an ordered range of all the user comments where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user comments
	 */
	public java.util.List<UserComment> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first user comment in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public UserComment findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Returns the first user comment in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns the last user comment in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public UserComment findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Returns the last user comment in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns the user comments before and after the current user comment in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param commentId the primary key of the current user comment
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public UserComment[] findByUuid_C_PrevAndNext(
			long commentId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Removes all the user comments where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of user comments where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching user comments
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the user comments where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @return the matching user comments
	 */
	public java.util.List<UserComment> findByComplianceId(long complianceId);

	/**
	 * Returns a range of all the user comments where complianceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param complianceId the compliance ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of matching user comments
	 */
	public java.util.List<UserComment> findByComplianceId(
		long complianceId, int start, int end);

	/**
	 * Returns an ordered range of all the user comments where complianceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param complianceId the compliance ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user comments
	 */
	public java.util.List<UserComment> findByComplianceId(
		long complianceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns an ordered range of all the user comments where complianceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param complianceId the compliance ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user comments
	 */
	public java.util.List<UserComment> findByComplianceId(
		long complianceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first user comment in the ordered set where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public UserComment findByComplianceId_First(
			long complianceId,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Returns the first user comment in the ordered set where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByComplianceId_First(
		long complianceId,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns the last user comment in the ordered set where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public UserComment findByComplianceId_Last(
			long complianceId,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Returns the last user comment in the ordered set where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByComplianceId_Last(
		long complianceId,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns the user comments before and after the current user comment in the ordered set where complianceId = &#63;.
	 *
	 * @param commentId the primary key of the current user comment
	 * @param complianceId the compliance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public UserComment[] findByComplianceId_PrevAndNext(
			long commentId, long complianceId,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Removes all the user comments where complianceId = &#63; from the database.
	 *
	 * @param complianceId the compliance ID
	 */
	public void removeByComplianceId(long complianceId);

	/**
	 * Returns the number of user comments where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @return the number of matching user comments
	 */
	public int countByComplianceId(long complianceId);

	/**
	 * Returns all the user comments where status = &#63;.
	 *
	 * @param status the status
	 * @return the matching user comments
	 */
	public java.util.List<UserComment> findByStatus(int status);

	/**
	 * Returns a range of all the user comments where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of matching user comments
	 */
	public java.util.List<UserComment> findByStatus(
		int status, int start, int end);

	/**
	 * Returns an ordered range of all the user comments where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user comments
	 */
	public java.util.List<UserComment> findByStatus(
		int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns an ordered range of all the user comments where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user comments
	 */
	public java.util.List<UserComment> findByStatus(
		int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first user comment in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public UserComment findByStatus_First(
			int status,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Returns the first user comment in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByStatus_First(
		int status,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns the last user comment in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public UserComment findByStatus_Last(
			int status,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Returns the last user comment in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByStatus_Last(
		int status,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns the user comments before and after the current user comment in the ordered set where status = &#63;.
	 *
	 * @param commentId the primary key of the current user comment
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public UserComment[] findByStatus_PrevAndNext(
			long commentId, int status,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Removes all the user comments where status = &#63; from the database.
	 *
	 * @param status the status
	 */
	public void removeByStatus(int status);

	/**
	 * Returns the number of user comments where status = &#63;.
	 *
	 * @param status the status
	 * @return the number of matching user comments
	 */
	public int countByStatus(int status);

	/**
	 * Returns all the user comments where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the matching user comments
	 */
	public java.util.List<UserComment> findByG_S(long groupId, int status);

	/**
	 * Returns a range of all the user comments where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of matching user comments
	 */
	public java.util.List<UserComment> findByG_S(
		long groupId, int status, int start, int end);

	/**
	 * Returns an ordered range of all the user comments where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user comments
	 */
	public java.util.List<UserComment> findByG_S(
		long groupId, int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns an ordered range of all the user comments where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user comments
	 */
	public java.util.List<UserComment> findByG_S(
		long groupId, int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first user comment in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public UserComment findByG_S_First(
			long groupId, int status,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Returns the first user comment in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByG_S_First(
		long groupId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns the last user comment in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public UserComment findByG_S_Last(
			long groupId, int status,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Returns the last user comment in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public UserComment fetchByG_S_Last(
		long groupId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns the user comments before and after the current user comment in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param commentId the primary key of the current user comment
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public UserComment[] findByG_S_PrevAndNext(
			long commentId, long groupId, int status,
			com.liferay.portal.kernel.util.OrderByComparator<UserComment>
				orderByComparator)
		throws NoSuchUserCommentException;

	/**
	 * Removes all the user comments where groupId = &#63; and status = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 */
	public void removeByG_S(long groupId, int status);

	/**
	 * Returns the number of user comments where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the number of matching user comments
	 */
	public int countByG_S(long groupId, int status);

	/**
	 * Caches the user comment in the entity cache if it is enabled.
	 *
	 * @param userComment the user comment
	 */
	public void cacheResult(UserComment userComment);

	/**
	 * Caches the user comments in the entity cache if it is enabled.
	 *
	 * @param userComments the user comments
	 */
	public void cacheResult(java.util.List<UserComment> userComments);

	/**
	 * Creates a new user comment with the primary key. Does not add the user comment to the database.
	 *
	 * @param commentId the primary key for the new user comment
	 * @return the new user comment
	 */
	public UserComment create(long commentId);

	/**
	 * Removes the user comment with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param commentId the primary key of the user comment
	 * @return the user comment that was removed
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public UserComment remove(long commentId) throws NoSuchUserCommentException;

	public UserComment updateImpl(UserComment userComment);

	/**
	 * Returns the user comment with the primary key or throws a <code>NoSuchUserCommentException</code> if it could not be found.
	 *
	 * @param commentId the primary key of the user comment
	 * @return the user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public UserComment findByPrimaryKey(long commentId)
		throws NoSuchUserCommentException;

	/**
	 * Returns the user comment with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param commentId the primary key of the user comment
	 * @return the user comment, or <code>null</code> if a user comment with the primary key could not be found
	 */
	public UserComment fetchByPrimaryKey(long commentId);

	/**
	 * Returns all the user comments.
	 *
	 * @return the user comments
	 */
	public java.util.List<UserComment> findAll();

	/**
	 * Returns a range of all the user comments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of user comments
	 */
	public java.util.List<UserComment> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the user comments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user comments
	 */
	public java.util.List<UserComment> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator);

	/**
	 * Returns an ordered range of all the user comments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of user comments
	 */
	public java.util.List<UserComment> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserComment>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the user comments from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of user comments.
	 *
	 * @return the number of user comments
	 */
	public int countAll();

	@Override
	public Set<String> getBadColumnNames();

}